{
  description = "Calculator using morse code on a keyboard with MIDI input/output, because why not?";

  outputs = { self, nixpkgs }: let
    forAllSystems = nixpkgs.lib.genAttrs [ "x86_64-linux" "x86_64-darwin" "i686-linux" "aarch64-linux" ];
  in {
    devShell = forAllSystems (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
    in pkgs.mkShell {
      buildInputs = with pkgs; [ portmidi ];
    });
  };
}
