use std::collections::HashMap;

#[derive(Hash, PartialEq, Eq, Debug)]
pub enum Key {
    Short, Long
}

pub use self::Key::*;

pub type Decoder = HashMap<&'static [Key], char>;
pub type Encoder = HashMap<char, &'static [Key]>;

pub fn decoder() -> Decoder {
    let mut map = Decoder::with_capacity(44);

    map.insert(&[Short, Long], 'a');
    map.insert(&[Long, Short, Short, Short], 'b');
    map.insert(&[Long, Short, Long, Short], 'c');
    map.insert(&[Long, Short, Short], 'd');
    map.insert(&[Short], 'e');
    map.insert(&[Short, Short, Long, Short], 'f');
    map.insert(&[Long, Long, Short], 'g');
    map.insert(&[Short, Short, Short, Short], 'h');
    map.insert(&[Short, Short], 'i');
    map.insert(&[Short, Long, Long, Long], 'j');
    map.insert(&[Long, Short, Long], 'k');
    map.insert(&[Short, Long, Short, Short], 'l');
    map.insert(&[Long, Long], 'm');
    map.insert(&[Long, Short], 'n');
    map.insert(&[Long, Long, Long], 'o');
    map.insert(&[Short, Long, Long, Short], 'p');
    map.insert(&[Long, Long, Short, Long], 'q');
    map.insert(&[Short, Long, Short], 'r');
    map.insert(&[Short, Short, Short], 's');
    map.insert(&[Long], 't');
    map.insert(&[Short, Short, Long, Long], 'u');
    map.insert(&[Short, Short, Short, Long, Long], 'v');
    map.insert(&[Short, Long, Long], 'w');
    map.insert(&[Long, Short, Short, Long], 'x');
    map.insert(&[Long, Short, Long, Long], 'y');
    map.insert(&[Long, Long, Short, Short], 'z');
    map.insert(&[Long, Long, Long, Long, Long], '0');
    map.insert(&[Short, Long, Long, Long, Long], '1');
    map.insert(&[Short, Short, Long, Long, Long], '2');
    map.insert(&[Short, Short, Short, Long, Long], '3');
    map.insert(&[Short, Short, Short, Short, Long], '4');
    map.insert(&[Short, Short, Short, Short, Short], '5');
    map.insert(&[Long, Short, Short, Short, Short], '6');
    map.insert(&[Long, Long, Short, Short, Short], '7');
    map.insert(&[Long, Long, Long, Short, Short], '8');
    map.insert(&[Long, Long, Long, Long, Short], '9');
    map.insert(&[Short, Long, Short, Long, Short, Long], '.');
    map.insert(&[Long, Short, Short, Long, Short], '/');
    map.insert(&[Long, Short, Long, Long, Short], '(');
    map.insert(&[Long, Short, Long, Long, Short, Long], ')');
    map.insert(&[Long, Short, Short, Short, Long], '=');
    map.insert(&[Short, Long, Short, Long, Short], '+');
    map.insert(&[Long, Short, Short, Short, Short, Long], '-');
    map.insert(&[Short, Short, Long, Long, Short, Long], '_');

    map
}
pub fn encoder() -> Encoder {
    let mut map = Encoder::with_capacity(44);

    map.insert('a', &[Short, Long]);
    map.insert('b', &[Long, Short, Short, Short]);
    map.insert('c', &[Long, Short, Long, Short]);
    map.insert('d', &[Long, Short, Short]);
    map.insert('e', &[Short]);
    map.insert('f', &[Short, Short, Long, Short]);
    map.insert('g', &[Long, Long, Short]);
    map.insert('h', &[Short, Short, Short, Short]);
    map.insert('i', &[Short, Short]);
    map.insert('j', &[Short, Long, Long, Long]);
    map.insert('k', &[Long, Short, Long]);
    map.insert('l', &[Short, Long, Short, Short]);
    map.insert('m', &[Long, Long]);
    map.insert('n', &[Long, Short]);
    map.insert('o', &[Long, Long, Long]);
    map.insert('p', &[Short, Long, Long, Short]);
    map.insert('q', &[Long, Long, Short, Long]);
    map.insert('r', &[Short, Long, Short]);
    map.insert('s', &[Short, Short, Short]);
    map.insert('t', &[Long]);
    map.insert('u', &[Short, Short, Long, Long]);
    map.insert('v', &[Short, Short, Short, Long, Long]);
    map.insert('w', &[Short, Long, Long]);
    map.insert('x', &[Long, Short, Short, Long]);
    map.insert('y', &[Long, Short, Long, Long]);
    map.insert('z', &[Long, Long, Short, Short]);
    map.insert('0', &[Long, Long, Long, Long, Long]);
    map.insert('1', &[Short, Long, Long, Long, Long]);
    map.insert('2', &[Short, Short, Long, Long, Long]);
    map.insert('3', &[Short, Short, Short, Long, Long]);
    map.insert('4', &[Short, Short, Short, Short, Long]);
    map.insert('5', &[Short, Short, Short, Short, Short]);
    map.insert('6', &[Long, Short, Short, Short, Short]);
    map.insert('7', &[Long, Long, Short, Short, Short]);
    map.insert('8', &[Long, Long, Long, Short, Short]);
    map.insert('9', &[Long, Long, Long, Long, Short]);
    map.insert('.', &[Short, Long, Short, Long, Short, Long]);
    map.insert('/', &[Long, Short, Short, Long, Short]);
    map.insert('(', &[Long, Short, Long, Long, Short]);
    map.insert(')', &[Long, Short, Long, Long, Short, Long]);
    map.insert('=', &[Long, Short, Short, Short, Long]);
    map.insert('+', &[Short, Long, Short, Long, Short]);
    map.insert('-', &[Long, Short, Short, Short, Short, Long]);
    map.insert('_', &[Short, Short, Long, Long, Short, Long]);

    map
}
