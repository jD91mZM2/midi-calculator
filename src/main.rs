#[macro_use] extern crate clap;

use clap::{Arg, App};
use failure::Error;
use portmidi::{PortMidi, OutputPort, MidiMessage};
use std::{
    char,
    thread,
    time::{Duration, Instant}
};

const BUFFER_SIZE: usize = 1024;
const STRENGTH_NORMAL: u8 = 100;
const KEY_DEFAULT: u8 = 72;

const STATUS_KEY: u8 = 0x80;
const STATUS_ON: u8 = 0x10;

mod morse;

use self::morse::{Key, Encoder};

fn main() -> Result<(), Error> {
    let matches = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(Arg::with_name("list")
            .short("l")
            .long("list")
            .help("Lists all midi devices"))
        .arg(Arg::with_name("input")
             .help("The input device")
             .required_unless("list"))
        .arg(Arg::with_name("output")
             .help("The output device")
             .required_unless("list"))
        .get_matches();

    let midi = PortMidi::new()?;

    if matches.is_present("list") {
        let devices = midi.devices()?;
        println!("Inputs:");
        for device in &devices {
            if device.is_input() {
                println!("{}", device);
            }
        }
        println!();
        println!("Outputs:");
        for device in &devices {
            if device.is_output() {
                println!("{}", device);
            }
        }
        return Ok(());
    }

    let input_id = matches.value_of("input").unwrap().parse();
    let output_id = matches.value_of("output").unwrap().parse();

    let (input_id, output_id) = match (input_id, output_id) {
        (Ok(input), Ok(output)) => (input, output),
        _ => {
            eprintln!("can't parse input as integer");
            return Ok(());
        }
    };

    let input = midi.device(input_id)?;
    let output = midi.device(output_id)?;

    if !input.is_input() || !output.is_output() {
        eprintln!("trying to use an output device as input or vise versa");
        return Ok(());
    }

    let mut input = midi.input_port(input, BUFFER_SIZE)?;
    let mut output = midi.output_port(output, BUFFER_SIZE)?;

    let encoder = morse::encoder();
    let decoder = morse::decoder();

    let mut key_start = None;
    let mut inactive = Instant::now();
    let mut current = Vec::new();

    let mut negative1 = false;
    let mut negative2 = false;
    let mut key1: i32 = 0;
    let mut operator = None;
    let mut key2: i32 = 0;

    loop {
        let event = match input.read()? {
            Some(event) => event.message,
            None => {
                thread::sleep(Duration::from_millis(250));

                if key_start.is_none() {
                    if inactive.elapsed().as_secs() == 2 && !current.is_empty() {
                        if let Some(&c) = decoder.get(&*current) {
                            println!("{}", c);

                            if c == '=' {
                                if negative1 { key1 = -key1; }
                                if negative2 { key2 = -key2; }

                                let result = match operator {
                                    Some('+') => key1 as f64 + key2 as f64,
                                    Some('-') => key1 as f64 - key2 as f64,
                                    Some('*') => key1 as f64 * key2 as f64,
                                    Some('/') => key1 as f64 / key2 as f64,
                                    _ => key1 as f64
                                };

                                println!("{} {:?} {} = {}", key1, operator, key2, result);

                                negative1 = false;
                                negative2 = false;
                                key1 = 0;
                                key2 = 0;
                                operator = None;

                                encode_num(&mut output, &encoder, result)?;
                            } else if key1 != 0 && (c == '+' || c == '-' || c == '/' || c == '*') {
                                if operator.is_some() {
                                    if c == '-' {
                                        negative2 = !negative2;
                                    } else {
                                        press(&mut output, 1000)?;
                                    }
                                } else {
                                    operator = Some(c);
                                }
                            } else if key1 == 0 && c == '-' {
                                negative1 = !negative1;
                            } else if c >= '0' && c <= '9' {
                                let key = if operator.is_none() { &mut key1 } else { &mut key2 };
                                *key *= 10;
                                *key += (c as u8 - b'0') as i32;
                            }
                        } else {
                            press(&mut output, 3000)?;
                            inactive = Instant::now();
                        }
                        current.clear();
                    }
                }
                continue;
            }
        };
        if event.status & (!STATUS_ON) != STATUS_KEY {
            continue;
        }

        println!("{:?}", event);
        inactive = Instant::now();

        if event.status & STATUS_ON == STATUS_ON {
            key_start = Some(Instant::now());
            continue;
        }

        if let Some(time) = key_start {
            current.push(if dbg!(time.elapsed()).as_millis() >= 500 {
                morse::Key::Long
            } else {
                morse::Key::Short
            });
            println!("{:?}", current);
            key_start = None;
        }
    }
}
fn press(output: &mut OutputPort, length: u64) -> Result<(), Error> {
    output.write_message(MidiMessage {
        status: STATUS_KEY | STATUS_ON,
        data1: KEY_DEFAULT,
        data2: STRENGTH_NORMAL,
        data3: 0,
    })?;
    thread::sleep(Duration::from_millis(length));
    output.write_message(MidiMessage {
        status: STATUS_KEY,
        data1: KEY_DEFAULT,
        data2: 0,
        data3: 0,
    })?;
    Ok(())
}
fn encode_char(output: &mut OutputPort, encoder: &Encoder, c: char) -> Result<(), Error> {
    println!("Encoding: {}", c);
    for key in encoder[&c] {
        press(output, match key {
            Key::Short => 250,
            Key::Long => 750
        })?;
    }
    thread::sleep(Duration::from_millis(250));
    Ok(())
}
fn encode_num(output: &mut OutputPort, encoder: &Encoder, mut number: f64) -> Result<(), Error> {
    if number.is_infinite() {
        for c in "infinity".chars() {
            encode_char(output, encoder, c)?;
        }
        return Ok(());
    }
    if number < 0.0 {
        // In our case, we shouldn't need to worry about overflow.
        // The lowest number out user can make is NOT std::f64::MIN
        number = -number;

        encode_char(output, encoder, '-')?;
    }
    if (number % 10.0).floor() == 0.0 {
        encode_char(output, encoder, '0')?;
    } else {
        let mut cursor = 10f64.powf(std::f64::MAX_10_EXP as f64);
        while cursor > 0.0 {
            let shifted = (number / cursor).floor();
            if shifted > 0.0 {
                encode_char(output, encoder, char::from_digit((shifted % 10.0) as u32, 10).unwrap())?;
            }
            cursor = (cursor / 10.0).floor();
        }
    }
    if number % 1.0 > 0.0 {
        encode_char(output, encoder, '.')?;

        while number % 1.0 > 0.0 {
            number *= 10.0;
            encode_char(output, encoder, char::from_digit((number % 10.0) as u32, 10).unwrap())?;
        }
    }
    Ok(())
}
