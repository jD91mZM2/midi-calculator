# midi-calculator

Calculator using morse code on a keyboard with MIDI input/output, because why not?

## Why?

> Because madness, as you know, is like gravity. All it takes it a little... push!

- Joker, The Dark Knight

I came home from school today, incredibly mad that it took so much of my time,
that I really wasn't feeling like doing anything complicated or useful anymore.
So I did this. You're welcome.
